#include<iostream>
using namespace std;

#ifndef Graph
#define Graph
#define ROW 7
#define COL 7
#define infi 5000  //infi for infinity

class Nodedata {
public:
	Nodedata() {
		next = NULL;
	}
	Nodedata(int el, char el2, Nodedata *n = 0) {
		cost = el;
		element = el2;
		next = n;
	}
	Nodedata *next;
	int cost;
	char element;
};

class edge {
public:
	edge() {
		next = NULL;
	}
	edge(char el2) {
		el = el2; node = 0; next = 0;
	}

	Nodedata *node;
	edge *next;
	char el;
};

class graph {
public:
	// constructor graph
	graph() {
		root = NULL;
	}
	void inputgraph(int **arr, int num);
	void showgraph(int number);
	bool multigraph();
	bool psudograph();
	bool digraph();
	bool weightedgraph();
	bool completegraph();
	void shortestpath(int size);
	edge *root;
};

class prims
{
	int graph[ROW][COL], nodes;
public:
	prims();
	void createGraph();
	void primsAlgo();
};



#endif