#include <iostream>
#include<iomanip>
#include "graph.h"

using namespace std;

void graph::inputgraph(int **arr, int num) {
	edge *p_edge = root;
	Nodedata *p_node = 0;

	for (int i = 0; i < num; i++) {
		if (i == 0) {
			root = new edge(i + 65);
			p_edge = root;
			p_node = p_edge->node;
		}
		else {
			p_edge->next = new edge(i + 65);
			p_edge = p_edge->next;
			p_node = p_edge->node;
		}

		for (int j = 0; j < num; j++) {
			if (p_node == NULL && arr[i][j] != -1) {
				p_edge->node = new Nodedata(arr[i][j], j + 65);
				p_node = p_edge->node;
			}
			else if (arr[i][j] != -1) {
				p_node->next = new Nodedata(arr[i][j], j + 65);
				p_node = p_node->next;
			}
		}
	}
};

// check multigraph
bool graph::multigraph() {
	edge *p_edge = root;
	Nodedata *p_node;
	while (p_edge != NULL) {
		p_node = p_edge->node;
		while (p_node != NULL) {
			edge *p_edge2 = p_edge->next;
			while (p_edge2 != NULL) {
				if (p_edge2->el == p_node->element) {
					Nodedata *p_node2 = p_edge2->node;
					while (p_node2 != NULL) {
						if (p_node2->element == p_edge->el) {
							return true;
						}
						p_node2 = p_node2->next;
					}
				}
				p_edge2 = p_edge2->next;
			}
			p_node = p_node->next;
		}
		p_edge = p_edge->next;
	}
	return false;
}

// check psudograph
bool graph::psudograph() {
	edge *p_edge = root;
	Nodedata *p_node;
	while (p_edge != NULL) {
		p_node = p_edge->node;
		while (p_node != NULL) {
			if (p_node->element == p_edge->el) {
				return true;
			}
			p_node = p_node->next;
		}
		p_edge = p_edge->next;
	}
	return false;
}

// check digraph
bool graph::digraph() {
	edge *p_edge = root;
	Nodedata *p_node;
	while (p_edge != NULL) {
		p_node = p_edge->node;
		while (p_node != NULL) {
			edge *p_edge2 = p_edge->next;
			while (p_edge2 != NULL) {
				if (p_edge2->el == p_node->element) {
					Nodedata *p_node2 = p_edge2->node;
					while (p_node2 != NULL) {
						if (p_node2->element == p_edge->el) {
							if (p_node->cost == p_node2->cost) {
								return false;
							}
						}
						p_node2 = p_node2->next;
					}
				}
				p_edge2 = p_edge2->next;
			}
			p_node = p_node->next;
		}
		p_edge = p_edge->next;
	}
	return true;
}

// check weightedgraph
bool graph::weightedgraph()
{
	edge *p_edge = root;
	Nodedata *p_node;
	while (p_edge != NULL) {
		p_node = p_edge->node;
		while (p_node != NULL) {
			if (p_node->cost != 0) {
				return true;
			}
			p_node = p_node->next;
		}
		p_edge = p_edge->next;
	}
	return false;
}

// check completegraph
bool graph::completegraph() {
	int count_node = 0;
	int sum_count_node = 0;
	int count_edge = 0;
	edge *p_edge = root;
	Nodedata *p_node;

	while (p_edge != NULL) {
		p_node = p_edge->node;
		count_node = 0;
		while (p_node != NULL) {
			if (p_node->element != p_edge->el)
				count_node++;
			p_node = p_node->next;
		}
		if (p_edge == root) {
			sum_count_node = count_node;
		}
		else {
			if (sum_count_node != count_node) {
				return false;
			}
		}
		count_edge++;
		p_edge = p_edge->next;
	}

	if (sum_count_node == count_edge - 1) {
		return true;
	}
	else {
		return false;
	}
}

// output graph
void graph::showgraph(int num) {
	edge *p_edge = root;
	Nodedata *p_node;
	cout << endl;
	int i;
	for (i = 0; i < num; i++)
	{
		char value = 65 + i;
		cout << value << " -> ";
		p_node = p_edge->node;
		while (p_node != 0) {

			char value2 = p_node->element;
			if (p_node->cost != 0) {
				cout << "[" << value2 << "->" << p_node->cost << "]" " -> ";
			}
			p_node = p_node->next;
			//if (p_node == 0) { cout << " -> "; }
		}
		cout << "[NULL]";
		cout << endl;
		if (i != num - 1) {
			cout << "|" << endl;
		}
		p_edge = p_edge->next;
	}
}

void graph::shortestpath(int size)
{
	Nodedata *p_node;
	edge *p_edge = root;
	int *chararr = new int[size];
	int cost = 0;
	int small = 0;
	char firstele;

	do {
		cout << "Input capital letter node to find shortest path : ";
		cin >> firstele;
	} while (firstele < 65 || firstele >= 65 + size);

	char elem = firstele;
	char ele;
	int round = 0;
	int **arr = new int *[size + 1];

	for (int i = 0; i < size + 1; ++i) {
		arr[i] = new int[size];
	}
	for (int i = 0; i < size + 1; i++) {
		for (int j = 0; j < size; j++) {
			arr[i][j] = 0;
			chararr[j] = 0;
		}
	}
	cout << endl;

	chararr[firstele - 65] = 1;

	while (round < size) {
		p_edge = root;
		for (edge *temp = root; temp->el != elem; temp = temp->next) {
			p_edge = temp->next;
		}

		p_node = p_edge->node;
		arr[0][round] = p_edge->el;

		while (p_node != NULL) {
			if (round == 0) {
				arr[p_node->element - 64][round] = p_node->cost;
			}
			else {
				if (arr[p_node->element - 64][round - 1] <= p_node->cost + cost && arr[p_node->element - 64][round - 1] != 0) {
					arr[p_node->element - 64][round] = arr[p_node->element - 64][round - 1];
				}
				else if (chararr[p_node->element - 65] == 0) {
					arr[p_node->element - 64][round] = p_node->cost + cost;
				}
			}
			p_node = p_node->next;
		}

		small = 0;
		if (round == 0) {
			for (int i = 1; i <= size; i++) {
				if (arr[i][round] != 0 && small == 0) {
					small = arr[i][round];
					ele = i + 64;
				}
				else if (arr[i][round] != 0 && small > arr[i][round]) {
					small = arr[i][round];
					ele = i + 64;
				}
			}
			chararr[ele - 65] = small;
			cost = small;
			cout << ele << endl;
		}
		else {
			for (int i = 1; i <= size; i++) {
				if (arr[i][round] == 0 && chararr[i - 1] == 0) {
					arr[i][round] = arr[i][round - 1];
				}
			}
			for (int i = 1; i <= size; i++) {
				if (arr[i][round] != 0 && small == 0) {
					small = arr[i][round];
					ele = i + 65;
				}
				else if (arr[i][round] != 0 && small > arr[i][round]) {
					small = arr[i][round];
					ele = i + 65;
				}
			}
			if (small != 0) {
				chararr[ele - 65] = small;
			}
			cost = small;
		}
		elem = ele;
		round++;
	}

	for (int i = 0; i < size; i++) {
		char alpha = arr[0][i];
		cout << setw(4) << alpha;
	}
	cout << endl;
	for (int i = 1; i < size + 1; i++) {
		for (int j = 0; j < size; j++) {
			cout << setw(4) << arr[i][j];
		}
		cout << endl;
	}

	cout << endl << "Shortest path is : ";
	char a;
	for (int i = 0; i < size; i++) {
		a = i + 65;
		cout << a << chararr[i] << " ";
	}
	cout << endl;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define ROW 7
#define COL 7
#define infi 5000  //infi for infinity

prims::prims()
{
	for (int i = 0; i<ROW; i++)
		for (int j = 0; j<COL; j++)
			graph[i][j] = 0;
}

void prims::createGraph()
{
	int i, j;
	cout << "Enter Total Nodes : ";
	cin >> nodes;
	cout << "Enter Adjacency Matrix : ";
	for (i = 0; i<nodes; i++)
		for (j = 0; j<nodes; j++)
			cin >> graph[i][j];

	//Assign infinity to all graph[i][j] where weight is 0.for(i=0;i<nodes;i++){
	for (j = 0; j<nodes; j++)
	{
		if (graph[i][j] == 0)
			graph[i][j] = infi;
	}
}


void prims::primsAlgo()
{
	int selected[ROW], i, j, ne; //ne for no. of edges
	int min, x, y;

	for (i = 0; i<nodes; i++)
		selected[i] = false;

	selected[0] = true;
	ne = 0;

	while (ne < nodes - 1)
	{
		min = infi;
		for (i = 0; i<nodes; i++)
		{
			if (selected[i] == true) {
				for (j = 0; j<nodes; j++) {
					if (selected[j] == false) {
						if (min > graph[i][j])
						{
							min = graph[i][j];
							x = i;
							y = j;
						}
					}
				}
			}
		}
		selected[y] = true;
		cout << "n" << x + 1 << " --> " << y + 1;
		ne = ne + 1;
	}

}
